package ru.pkonovalov.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.entity.IHasName;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByName implements Comparator<IHasName> {

    private static @NotNull
    final ComparatorByName INSTANCE = new ComparatorByName();

    public static @NotNull ComparatorByName getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasName o1, @Nullable final IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getName() == null) return -1;
        if (o2.getName() == null) return 1;
        return o1.getName().compareTo(o2.getName());
    }

}

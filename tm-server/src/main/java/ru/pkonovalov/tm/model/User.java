package ru.pkonovalov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    private boolean Locked = false;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String login;

    @Nullable
    private String middleName;

    @Nullable
    private String passwordHash;

    @Nullable
    private Role role = Role.USER;

}

package ru.pkonovalov.tm.exception.empty;

import ru.pkonovalov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}

package ru.pkonovalov.tm.exception.user;

import ru.pkonovalov.tm.exception.AbstractException;

public class AlreadyLoggedInException extends AbstractException {

    public AlreadyLoggedInException() {
        super("Error! You are already logged in...");
    }

}

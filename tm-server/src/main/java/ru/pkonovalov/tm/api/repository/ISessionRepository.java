package ru.pkonovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.api.IRepository;
import ru.pkonovalov.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

    List<Session> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

}

package ru.pkonovalov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.exception.entity.RoleNotFoundException;

import static ru.pkonovalov.tm.util.ValidationUtil.checkRole;

@Getter
public enum Role {

    USER("User"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static @NotNull Role getRole(String s) {
        s = s.toUpperCase();
        if (!checkRole(s)) throw new RoleNotFoundException();
        return valueOf(s);
    }

}

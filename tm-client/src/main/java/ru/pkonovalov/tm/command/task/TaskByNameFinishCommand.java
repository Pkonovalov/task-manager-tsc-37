package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class TaskByNameFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish task by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-finish-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        endpointLocator.getTaskEndpoint().finishTaskByName(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}

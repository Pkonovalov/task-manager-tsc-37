package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class ProjectByNameViewCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "View project by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-view-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        showProject(endpointLocator.getProjectEndpoint().findProjectByName(endpointLocator.getSession(), TerminalUtil.nextLine()));
    }

}

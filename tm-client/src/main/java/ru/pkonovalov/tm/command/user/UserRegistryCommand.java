package ru.pkonovalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.empty.EmptyEmailException;
import ru.pkonovalov.tm.exception.user.EmailExistsException;
import ru.pkonovalov.tm.exception.user.LoginExistsException;
import ru.pkonovalov.tm.util.TerminalUtil;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Register new user";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-registry";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        if (endpointLocator.getUserEndpoint().existsUserByLogin(login)) throw new LoginExistsException();
        System.out.println("ENTER E-MAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (endpointLocator.getUserEndpoint().existsUserByEmail(email)) throw new EmailExistsException();
        System.out.println("ENTER PASSWORD:");
        endpointLocator.getUserEndpoint().registryUser(login, TerminalUtil.nextLine(), email);
    }

}

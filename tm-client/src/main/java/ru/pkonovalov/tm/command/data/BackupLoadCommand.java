package ru.pkonovalov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;

public final class BackupLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Load data from backup";
    }

    @NotNull
    @Override
    public String commandName() {
        return "backup-load";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BACKUP LOAD]");
        endpointLocator.getAdminEndpoint().loadBackup(endpointLocator.getSession());
    }

}

package ru.pkonovalov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;
import ru.pkonovalov.tm.endpoint.User;
import ru.pkonovalov.tm.exception.entity.UserNotFoundException;

import static ru.pkonovalov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        if (user.getRole() != null)
            System.out.println("ROLE: " + user.getRole());
        System.out.print(dashedLine());
    }

}

package ru.pkonovalov.tm;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pkonovalov.tm.endpoint.AdminEndpoint;
import ru.pkonovalov.tm.marker.SoapCategory;

public class AdminEndpointTest extends AbstractEndpointTest {

    @NotNull
    private static final AdminEndpoint ADMIN_ENDPOINT = BOOTSTRAP.getAdminEndpoint();

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void denyLoadBackupTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.loadBackup(SESSION);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void denyLoadJsonTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.loadJson(SESSION);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void denySaveBackupTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.saveBackup(SESSION);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void denySaveJsonTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.saveJson(SESSION);
    }

    @After
    public void finishTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
    }

    @Test
    @Category(SoapCategory.class)
    public void runTest() {
        ADMIN_ENDPOINT.saveJson(SESSION);
        ADMIN_ENDPOINT.loadJson(SESSION);
        ADMIN_ENDPOINT.saveBackup(SESSION);
        ADMIN_ENDPOINT.loadBackup(SESSION);
    }

    @Before
    public void startTest() {
        SESSION = SESSION_ENDPOINT.openSession(ADMIN_USER_NAME, ADMIN_USER_PASSWORD);
    }

}
